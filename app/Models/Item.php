<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
   	protected $table = 'item';

    protected $fillable =['invoice_id','name','noOfItme','price','total','created_at','updated_at'];
}
