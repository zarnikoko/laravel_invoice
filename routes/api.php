<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/store_invoice','ApiInvoiceController@store_inv')->name('store_inv');

Route::get('/get_invoices','ApiInvoiceController@get_invs')->name('get_invs');

Route::get('/edit_invoice','ApiInvoiceController@edit_inv')->name('edit_inv');

Route::put('/update_invoice','ApiInvoiceController@update_inv')->name('update_inv');

Route::delete('/invoice','ApiInvoiceController@destory_inv')->name('del_inv');

Route::get('/pdf', 'ApiDynamicPDFController@pdf')->name('api_pdf');

Route::get('/excel','ApiExcelController@export_excel')->name('api_export_excel');
