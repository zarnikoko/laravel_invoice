<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use App\Models\Invoice;
use App\Models\Item;

class ApiExcelController extends Controller
{
    public function export_excel(Request $request){
    	$id = $request->id;
    	$invoice = Invoice::findOrFail($id);
    	$date    = date("Y_m_d_His");
    	$filename = $date."_".$invoice->name.".xls";
    	$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setTitle($invoice->name);
		$sheet->mergeCells('A1:E1');
		$style = array(
			'fontsize' => 22,
			'h_align' =>"CENTER",
			'rowheight' => 50,
			'v_align' => "TOP"
		);
		$this->setCell($sheet,1,0,"Invoice Name : ".$invoice->name,$style,'A1:E1');

		$style = array(
			'fontsize' => 16,
			'h_align' =>"CENTER",
			'rowheight' => 35
		);
		$this->setCell($sheet,2,0,"No.",$style);
		$this->setCell($sheet,2,1,"Item Name",$style);
		$this->setCell($sheet,2,2,"# of Items ",$style);
		$this->setCell($sheet,2,3,"Price",$style);
		$this->setCell($sheet,2,4,"Total",$style);

		$rowCount = 3;
		$num = 1;
		foreach($invoice->items as $item){
			$style = array(
				'fontsize' => 14,
				'h_align' =>"CENTER",
				'rowheight' => 32
			);
			$this->setCell($sheet,$rowCount,0,$num,$style);
			$style['h_align'] = "LEFT";
			$this->setCell($sheet,$rowCount,1,$item->name,$style);
			$style['h_align'] = "CENTER";
			$this->setCell($sheet,$rowCount,2,$item->noOfItem,$style);
			$style['h_align'] = "RIGHT";
            if($item->price-floor($item->price)>0){
                $sheet->getStyle('D'.$rowCount)->getNumberFormat()->setFormatCode('#,##0.00');
            }else{
                $sheet->getStyle('D'.$rowCount)->getNumberFormat()->setFormatCode('#,##0');
            }
            if($item->total-floor($item->total)>0){
                $sheet->getStyle('E'.$rowCount)->getNumberFormat()->setFormatCode('#,##0.00');
            }else{
                $sheet->getStyle('E'.$rowCount)->getNumberFormat()->setFormatCode('#,##0');
            }
			$this->setCell($sheet,$rowCount,3,$item->price,$style);
			$this->setCell($sheet,$rowCount,4,$item->total,$style);
			$rowCount++;
			$num++;
		}

		$style['h_align'] ="CENTER";
        $taxRowCount = $rowCount+1;
        $gtotalRowCount = $rowCount+2;
        if($invoice->sub_total-floor($invoice->sub_total)>0){
            $sheet->getStyle('E'.$rowCount)->getNumberFormat()->setFormatCode('#,##0.00');
        }else{
            $sheet->getStyle('E'.$rowCount)->getNumberFormat()->setFormatCode('#,##0');
        }
        if($invoice->tax-floor($invoice->tax)>0){
            $sheet->getStyle('E'.$taxRowCount)->getNumberFormat()->setFormatCode('#,##0.00');
        }else{
            $sheet->getStyle('E'.$taxRowCount)->getNumberFormat()->setFormatCode('#,##0');
        }
        if($invoice->grand_total-floor($invoice->grand_total)>0){
            $sheet->getStyle('E'.$gtotalRowCount)->getNumberFormat()->setFormatCode('#,##0.00');
        }else{
            $sheet->getStyle('E'.$gtotalRowCount)->getNumberFormat()->setFormatCode('#,##0');
        }
		$this->setCell($sheet,$rowCount,3,'Sub Total',$style);
		$style['h_align'] ="RIGHT";
		$this->setCell($sheet,$rowCount,4,$invoice->sub_total,$style);
		$style['h_align'] ="CENTER";
		$this->setCell($sheet,$rowCount+1,3,'Tax',$style);
		$style['h_align'] ="RIGHT";
		$this->setCell($sheet,$rowCount+1,4,$invoice->tax,$style);
		$style['h_align'] ="CENTER";
		$this->setCell($sheet,$rowCount+2,3,'Grand Total',$style);
		$style['h_align'] ="RIGHT";
		$this->setCell($sheet,$rowCount+2,4,$invoice->grand_total,$style);

		$sheet->getColumnDimension('A')->setWidth(5);
		$sheet->getColumnDimension('B')->setWidth(20);
		$sheet->getColumnDimension('C')->setWidth(20);
		$sheet->getColumnDimension('D')->setWidth(20);
		$sheet->getColumnDimension('E')->setWidth(20);
		// $sheet->getStyle("A1")->getAlignment()->setHorizontal('CENTER');

		$writer = new Xlsx($spreadsheet);
		$response =  new StreamedResponse(
            function () use ($writer) {
                $writer->save('php://output');
            }
        );
        $response->headers->set('Content-Type', 'application/vnd.ms-excel;charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename="'.$filename.'"');
        $response->headers->set('Cache-Control','max-age=0');
        return $response;
    }

    public function setCell($sheet, $rowCount, $colCount, $value, $style=array(), $margecell= null) {
        //$sheet->getRowDimension($rowCount)->setRowHeight(15.5);
        // $sheet->getStyleByColumnAndRow($colCount, $rowCount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $h_align = "LEFT";
        $fontsize = 12;
        $border_style="thin";
        $rowheight = 20;
        $v_align = "center";
     	if(array_key_exists('h_align', $style)){
     		$h_align = $style['h_align'];
     	}
     	if(array_key_exists('border_style', $style)){
     		$border_style = $style['border_style'];
     	}
     	if(array_key_exists('fontsize', $style)){
     		$fontsize = $style['fontsize'];
     	}
     	if(array_key_exists('rowheight', $style)){
     		$rowheight = $style['rowheight'];
     	}
     	if(array_key_exists('v_align', $style)){
     		$v_align = $style['v_align'];
     	}
        $colName = chr(65 + $colCount);
        $cell = $colName.$rowCount;
        if(!is_null($margecell)){
        	$cell = $margecell;
        }
        // var_dump($cell);
        // exit();
        $sheet->getStyle($cell)->getBorders()->getTop()->setBorderStyle($border_style);
        $sheet->getStyle($cell)->getBorders()->getBottom()->setBorderStyle($border_style);
        $sheet->getStyle($cell)->getBorders()->getRight()->setBorderStyle($border_style);
        $sheet->getStyle($cell)->getBorders()->getLeft()->setBorderStyle($border_style);
       	$sheet->getStyle($cell)->getFont()->setSize($fontsize);
       	$sheet->getStyle($cell)->getAlignment()->setHorizontal($h_align);
       	if($rowheight>$fontsize+7){
       		$sheet->getRowDimension($rowCount)->setRowHeight($rowheight);
       		$sheet->getStyle($cell)->getAlignment()->setVertical($v_align);
       	}
        if(is_null($margecell)){
        	$sheet->setCellValue($cell,$value);
        }else {
           	$sheet->setCellValue($colName.$rowCount,$value); 
    	}
    }
}
