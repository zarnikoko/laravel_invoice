$(document).ready(function(){
  $('#show_table').DataTable({
    "columnDefs": [ {
          "targets": 'no-sort',
          "orderable": false,
            } ]
            ,
          "bInfo" : false,
          "bLengthChange" : false,
          "pageLength": 10,
          "aaSorting": []
  });
  $("#show_table_wrapper" ).prepend($( "#show_table_length"));
  $("#show_table_info").insertBefore($( "#show_table_paginate"));

});
