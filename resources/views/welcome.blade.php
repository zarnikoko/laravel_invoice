<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" rel="stylesheet"/>
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        
<div class="container">
    <form action="#" autocomplete="nope">
    <div class="form-row">
        <div class="form-group col-md-4">
            <label for="companyName" class="mr-2 col-form-label-sm">Name:&nbsp;</label>
            <input type="text" class="form-control form-control-sm" autocomplete="off" name="companyName" id="companyName" placeholder="Company Name" required="" pattern="^.+$" value="">
        </div>
        <div class="form-group col-md-4">
            <label for="version" class="mr-2 col-form-label-sm">Version:&nbsp;</label>
            <input type="text" class="form-control form-control-sm" autocomplete="off" name="version" id="version" placeholder="Version" required="" pattern="^.+$" value="">
        </div>
        <div class="form-group col-md-4">
            <label for="notes" class="mr-2 col-form-label-sm">Notes:&nbsp;</label>
            <input type="text" class="form-control form-control-sm" autocomplete="off" name="notes" id="notes" placeholder="Notes" pattern="^.+$" value="">
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-lg-12">
            <button type="submit" class="btn btn-primary btn-sm">Add</button>
            <button type="button" class="btn btn-light btn-sm ml-1">Cancel</button>
        </div>
    </div>
</form>
</div>
    </body>
</html>
