<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Invoice</title>

        <!-- link -->
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/all.css') }}">
        <!--script-->
        <script src="{{ URL::asset('js/jquery.js') }}"></script>
        <script src="{{ URL::asset('js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ URL::asset('js/newInvoice.js') }}"></script>

        <!-- Styles -->
        <style>
        </style>
    </head>
    <body>
    <div class="container" id="main_wrapper">
        @if(session('info'))
            <div class="alert alert-success alert-dismissable">
                <a class="panel-close close" data-dismiss="alert">×</a> 
                    {{session('info')}}
            </div>
        @endif
        <form action="{{route('invoice.update',$invoice->id)}}" method="post">
            @csrf
             {{method_field('PATCH')}}
            <div class="form-row mt-1">
                <div class="form-group col-md" style="display:flex;align-items:center;">
                    <label for="itemName" class="mr-2 btn-info btn"
                    onclick="location.reload()">
                        <i class="fas fa-edit"></i>Edit Invoice&nbsp;
                    </label>
                     <label for="itemName" class="mr-2 btn-primary btn" 
                     onclick="window.location.href='{{route('index')}}'">
                        <i class="fas fa-eye"></i>View Invoices&nbsp;
                    </label>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md" style="display:flex;align-items:center;">
                    <label for="itemName" class="mr-2 col-form-label">
                        Invoice Name&nbsp;
                    </label>
                    <input type="text" value="{{$invoice->name}}" class="form-control form-control-sm col-sm-3" autocomplete="off" name="invoiceName"  placeholder="Invoice Name" required>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md mr-5">
                    <label for="itemName" class="mr-2 col-form-label-sm">
                        Item Name&nbsp;
                    </label>
                </div>
                <div class="form-group col-md mr-5">
                    <label for="noOfItem" class="mr-2 col-form-label-sm">
                        # of Items&nbsp;
                    </label>
                </div>
                <div class="form-group col-md mr-5">
                    <label for="Price" class="mr-2 col-form-label-sm">
                        Price&nbsp;
                    </label>
                </div>
                <div class="form-group col-md mr-5">
                    <label for="" class="mr-2 col-form-label-sm">
                        Total&nbsp;
                    </label>
                </div>
                <div class="form-group col-md mr-5">
                    
                </div>
            </div>
            @foreach($invoice->items as $item)
            <div class="form-row cloneMe" id="cloneMe">
                <div class="form-group col-md mr-5">
                    <input type="text" value="{{$item->name}}" class="form-control form-control-sm" autocomplete="off" name="itemName[]"  placeholder="Item Name" required>
                </div>
                <div class="form-group col-md mr-5">
                    <input type="number" value="{{$item->noOfItem}}" class="form-control form-control-sm noOfItem" autocomplete="off" name="noOfItems[]"  placeholder="# of Items" required
                    min=0>
                </div>
                <div class="form-group col-md mr-5">
                    <input type="number" value="{{$item->price}}" class="form-control form-control-sm price" autocomplete="off" name="price[]" placeholder="Price" required 
                    step=0.01 min=0>
                </div>
                <div class="form-group col-md mr-5">
                    <input type="number" value="{{$item->total}}" class="form-control form-control-sm total" autocomplete="off" name="total[]" placeholder="Total" required readonly 
                    style="background:white;" step=0.01>
                </div>
                <div class="form-group col-md mr-5">
                    <button class="btn btn-danger pr-1 pl-1 pt-0 pb-0" title="Remove"
                    onclick="sub(this)" type="button">
                        <i class="fas fa-trash-alt"></i>
                    </button>
                </div>
            </div>
            @endforeach
            <div class="form-row" id="cloneAbove">
                <div class="form-group col-lg-12">
                   <button type="button" class="btn btn-primary btn-sm" id="addItem">
                        <i class="fas fa-plus"></i>Add Item(<spam id="countItem"></spam>)
                   </button>
                </div>
            </div>
            <div class="form-row mt-1">
                <div class="form-group col-md">
                   
                </div>
                <div class="form-group col-md">
                   
                </div>
                <div class="form-group col-md text-right">
                     <label for="itemName" class="mr-2 col-form-label-sm">
                        Sub Total&nbsp;
                    </label>
                </div>
                <div class="form-group col-md">
                    <input type="number" value="{{$invoice->sub_total}}" 
                    class="form-control form-control-sm"  
                    name="subTotal" id="subTotal" required readonly 
                    style="background:white;" step=0.01> 
                </div>
                <div class="form-group col-md">
                    
                </div>
            </div>
             <div class="form-row mt">
                <div class="form-group col-md">
                   
                </div>
                <div class="form-group col-md">
                   
                </div>
                <div class="form-group col-md text-right">
                     <label for="itemName" class="mr-2 col-form-label-sm">
                        Tax&nbsp;
                    </label>
                </div>
                <div class="form-group col-md">
                    <input type="number" value="{{$invoice->tax}}" 
                    class="form-control form-control-sm"  
                    name="tax" id="tax" required min=0 step=0.01> 
                </div>
                <div class="form-group col-md">
                    
                </div>
            </div>
            <div class="form-row mt">
                <div class="form-group col-md">
                   
                </div>
                <div class="form-group col-md">
                   
                </div>
                <div class="form-group col-md text-right">
                     <label for="itemName" class="mr-2 col-form-label-sm">
                        Grand Total&nbsp;
                    </label>
                </div>
                <div class="form-group col-md">
                    <input type="number" value="{{$invoice->grand_total}}" 
                    class="form-control form-control-sm"  
                    name="gTotal" id="gTotal" required readonly style="background:white;"
                    step=0.01> 
                </div>
                <div class="form-group col-md">
                    
                </div>
            </div>
             <div class="form-row" id="cloneAbove">
                <div class="form-group col-lg-12">
                   <button class="btn btn-info btn-sm" type="submit">
                      <i class="fas fa-arrow-circle-up"></i>Update
                   </button>
                    <button class="btn btn-light btn-sm" type="reset">
                      <i class="fas fa-redo-alt"></i>Reset
                   </button>
                </div>
            </div>
        </form>
    </div>
    </body>
</html>
