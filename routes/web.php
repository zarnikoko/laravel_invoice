<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::resource('invoice', 'invoiceController');
Route::get('/','invoiceController@index')->name('index');
Route::get('/pdf/{id}', 'DynamicPDFController@pdf')->name('pdf');
Route::get('/excel/{id}','ExcelController@export_excel')->name('export_excel');
