<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Invoice</title>

        <!-- link -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/jquery.dataTables.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/datatable.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/jquery.dataTables_themeroller.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/all.css') }}">
        <!--script-->
        <script src="{{ URL::asset('js/jquery.js') }}"></script>
        <script src="{{ URL::asset('js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ URL::asset('js/newInvoice.js') }}"></script>
       <!--  <script src="{{ URL::asset('js/jquery.dataTables.js') }}"></script>
        <script src="{{ URL::asset('js/jquery.dataTables.min.js') }}"></script> -->
        <script src="{{ URL::asset('js/index.js') }}"></script>
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                margin: 0;
            }

            .f-style{
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 100;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .code {
                border-right:2px solid;
                font-size: 26px;
                padding: 0 15px 0 15px;
                text-align: center;
            }

            .message {
                font-size: 18px;
                text-align: center;
            }
            .t-underline{
                text-decoration:underline;
            }
            .dataTables_wrapper .dataTables_filter{
                float:left;
                text-align:left;
                margin-bottom:15px;
            }
            .dataTables_length{
                float:right;
                margin-top:15px;
                margin-bottom:-15px;
            }
            .dataTables_info {
                clear: both;
                float: left;
                padding-top: 0.55em;
            }
        </style>
    </head>
    <body>
    <div class="container" id="main_wrapper">
        @if(session('info'))
            <div class="alert alert-success alert-dismissable">
                <a class="panel-close close" data-dismiss="alert">×</a> 
                    {{session('info')}}
            </div>
        @endif
        @if(isset($invoice))
            @if(sizeof($invoice)>0)
        <div id="show_table_length" class="dataTables_length">
            <a href="{{route('invoice.create')}}" class="btn btn-primary"> 
                <i class="fas fa-plus"></i>Add Invoice
            </a>
        </div>
        <div id="show_table_info" class="dataTables_info">
            <label>
                Total Invoice : {{$invoice->total()}}
            </label>
        </div>
        <div>
            <table id="show_table" class="table table-striped" 
            style="white-space:nowrap;table-layout:fixed;">
                <thead class="thead-dark">
                    <tr>
                        <th>Invoice Name</th>
                        <th>#of Items</th>
                        <th>Total</th>
                        <th style="width:7%"></th>
                        <th style="width:7%"></th>
                        <th style="width:7%"></th>
                        <th style="width:10%"></th>
                    </tr>
                </thead>
                <tbody>
                  @foreach($invoice as $i)
                    <tr>
                        <td style="vertical-align:middle;">
                          <a href="{{url('/invoice',$i->id)}}" class="t-underline" 
                            target="_blank">
                              {{$i->name}}
                          </a>
                        </td>
                        <td style="vertical-align:middle;">{{sizeof($i->items)}}</td>
                        <td style="vertical-align:middle;">{{$i->grand_total}}</td>
                        <td class="border" style="text-align:center;vertical-align:middle;">
                            <!-- <a  href="{{url('/pdf',$i->id)}}"
                                class="t-underline text-success" 
                                target="_blank"> -->
                            <a class="t-underline text-success" 
                               href="{{url('/pdf',$i->id)}}"
                               onclick="window.open(this.href, 'newwindow', 'width=850, height=950'); 
                               return false;">
                                    PDF
                            </a>
                        </td>
                         <td class="border" style="text-align:center;vertical-align:middle;">
                            <a  href="{{url('/excel',$i->id)}}"
                                class="t-underline text-success">
                                    Excel
                            </a>
                        </td>
                        <td class="border" style="text-align:center;vertical-align:middle;">
                             <a href="{{route('invoice.edit',$i->id)}}" class="t-underline">
                                    Edit
                            </a>
                        </td>
                        <td class="border" style="vertical-align:middle;">
                            <form method="post" action="{{route('invoice.destroy',$i->id)}}" onsubmit="return confirm('Are you sure to delete \'{{$i->name}}\' invoice?');">
                                @csrf
                                {{method_field('delete')}}
                                <button class="t-underline text-danger btn btn-link">
                                    Remove
                                </button>
                            </form>
                        </td>
                    </tr>
                  @endforeach
                </tbody>
            </table>
            
        </div>
        <div style="text-align:center" class="mt-5">
            <div style="display:inline-block;">
                {{ $invoice->links() }}
            </div>
        </div>
            @endif
        @endif
        @if(isset($invoice))
            @if(sizeof($invoice)==0)
        <div class="flex-center position-ref full-height f-style">
            <div class="code">
                No Invoice Found ?
            </div>
            <div class="message" style="padding:10px">
                <a href="{{route('invoice.create')}}" class="btn btn-primary"> 
                    <i class="fas fa-plus"></i>Add First Invoice
                </a>          
            </div>
        </div>
            @endif
        @endif
    </div>
    </body>
</html>
