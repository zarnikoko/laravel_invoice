$(document).ready(function(){
	 var tempTotal=0;
	 var realTotal=0;
	 countItem();
	 
	$('#addItem').click(function(){
		$("#cloneMe").clone().insertBefore( $( "#cloneAbove" ) ).find("input[type='number'], input[type='text']").val("");
		window.scrollTo(0,document.body.scrollHeight); // keep scrollbar at bottom
		countItem();
	}); // when click addItem btn add row for add item by cloning div and reset clone input value 

	sub = function(sub){   //to remove row by div class cloneMe
		var count = $('.cloneMe').length;
	    if (count != 1){ 
	    	sub.closest(".cloneMe").remove();
	    }
		else{ // prevent remove when item row remain one
			alert("Can't remove anymore!");
		}
		countItem();
		subtotal_cal();// call also Sub Total Calculation
		grandtotal_cal();// call also Grand Total Calculation
	}
	function subtotal_cal(){
      	$(".total").each(function(){
      			tempTotal+= Number(this.value);
      			realTotal=tempTotal;
       	});
       	if(realTotal!=0){
       		$("#subTotal").val(realTotal);
       	}
       	else{
       		$("#subTotal").val("");
       	}
      	tempTotal=0;
  	}
  	function grandtotal_cal(){
        var tax=$("#tax").val();
        var subTotal=$("#subTotal").val();
            if(subTotal!=""){
            	var tax_res = (tax/100)*subTotal; // by percentage
            	var res          = +subTotal + +tax_res;
            	$("#gTotal").val(res);
            }
            else{
            	$("#gTotal").val("");
            }
  	}
  	function countItem(){
  		var count = $('.cloneMe').length;
		$("#countItem").html(count);
  	}//to show number of item to add in Add item Button

	$( document ).on( "keyup", ".noOfItem,.price", function() { // total calculation on keyup no of item and price

	   var noOfItem =this.closest('.cloneMe').querySelector('.noOfItem').value;
	   var   price  =this.closest('.cloneMe').querySelector('.price').value;

	   	if(noOfItem!="" && price!=""){
			this.closest('.cloneMe').querySelector('.total').value=noOfItem * price; 	
		}
		else{
			this.closest('.cloneMe').querySelector('.total').value="";
		}

		subtotal_cal(); // call also Sub Total Calculation
		grandtotal_cal();// call also Grand Total Calculation

	}); // total calculation on keyup no of item and price

	$("#tax").on("keyup",function(){
		grandtotal_cal();
	});
});