<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Invoice;
use App\Models\Item;

class DynamicPDFController extends Controller
{
  
  function index()
  {
     //
  }

  function get_invoice_data($id)
  {
     	$invoice_data = Invoice::findOrFail($id);
     	return $invoice_data;
  }

  function pdf($id)
  {
  	 $invoice = Invoice::findOrFail($id);
  	 date_default_timezone_set('Asia/Yangon'); 
  	 // date_default_timezone_set('UTC');
  	 $date    = date("Y_m_d_His");
     $pdf = \App::make('dompdf.wrapper');
     $pdf->loadHTML($this->convert_invoice_data_to_html($id))->setPaper('a4')->setWarnings(false);
     //return $pdf->download($date.'_'.$invoice->name.'.pdf');
     return $pdf->stream($date.'_'.$invoice->name.'.pdf',array('Attachment'=> false));
  }

  function convert_invoice_data_to_html($id)
  {
     $date    = date("Y_m_d_His");
     $invoice_data = $this->get_invoice_data($id);
     $output = '
     <title>'.$date.'_'.$invoice_data->name.'.pdf </title>
     <h3 align="center">Invoice Name : '.$invoice_data->name.'</h3>
     	<table width="100%" style="border-collapse: collapse; border:0px;margin-top:50px;">
      		<tr>
    			<th style="border: 1px solid; padding:12px;text-align:center;vertical-align:middle;" width="25%">Item Name</th>
    			<th style="border: 1px solid; padding:12px;text-align:center;vertical-align:middle;" width="25%"># of Items</th>
    			<th style="border: 1px solid; padding:12px;text-align:center;vertical-align:middle;" width="25%">Price</th>
    			<th style="border: 1px solid; padding:12px;text-align:center;vertical-align:middle;" width="25%">Total</th>
   			</tr>
     	';  
     foreach($invoice_data->items as $item)
     {
      $output .= '
      		<tr>
       			<td style="border: 1px solid; padding:12px;text-align:center;vertical-align:middle;">'.$item->name.'</td>
       			<td style="border: 1px solid; padding:12px;text-align:center;vertical-align:middle;">'.$item->noOfItem.'</td>
       			<td style="border: 1px solid; padding:12px;text-align:center;vertical-align:middle;">'.$item->price.'</td>
       			<td style="border: 1px solid; padding:12px;text-align:center;vertical-align:middle;">'.$item->total.'</td>
      		</tr>
      ';
     }
     $output .= '</table>';
     $output .= '<table width="100%" style="border-collapse:collapse;border:0px;margin-top:30px;margin-bottom:30px;">
     		<tr>
    			<td></td>
    			<td></td>
    			<td style="border: 1px solid; padding:12px;text-align:center;vertical-align:middle;" width="25%">Sub Total</td>
    			<td style="border: 1px solid; padding:12px;text-align:center;vertical-align:middle;" width="25%">'.
    			$invoice_data->sub_total.'</td>
   			</tr>
   			<tr>
    			<td></td>
    			<td></td>
    			<td style="border: 1px solid; padding:12px;text-align:center;vertical-align:middle;" width="25%">Tax</td>
    			<td style="border: 1px solid; padding:12px;text-align:center;vertical-align:middle;" width="25%">'.
    			$invoice_data->tax.'</td>
   			</tr>
   			<tr>
    			<td></td>
    			<td></td>
    			<td style="border: 1px solid; padding:12px;text-align:center;vertical-align:middle;" width="25%">Grand Total</td>
    			<td style="border: 1px solid; padding:12px;text-align:center;vertical-align:middle;" width="25%">'.
    			$invoice_data->grand_total.'</td>
   			</tr>
   			</table>
     ';
     return $output;
   }

}
