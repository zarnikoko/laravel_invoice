<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Invoice</title>

        <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/all.css') }}">
        <!--script-->
        <script src="{{ URL::asset('js/jquery.js') }}"></script>
        <script src="{{ URL::asset('js/bootstrap.bundle.min.js') }}"></script>

        <!-- Styles -->
        <style>
        </style>
    </head>
    <body>
    <div class="container" id="main_wrapper">
        @if(session('info'))
            <div class="alert alert-success alert-dismissable">
                <a class="panel-close close" data-dismiss="alert">×</a> 
                    {{session('info')}}
            </div>
        @endif
        <h3 align="center">
            <span class="text-primary">Invoice Name : </span>
            <span class="text-warning">{{$invoice_data->name}}</span>
        </h3>
        <table width="100%" class="mt-5" style="border-collapse: collapse; border: 0px;">
            <tr>
                <th style="border: 1px solid; padding:12px;text-align:center;vertical-align:middle;" width="25%">Item Name</th>
                <th style="border: 1px solid; padding:12px;text-align:center;vertical-align:middle;" width="25%"># of Items</th>
                <th style="border: 1px solid; padding:12px;text-align:center;vertical-align:middle;" width="25%">Price</th>
                <th style="border: 1px solid; padding:12px;text-align:center;vertical-align:middle;" width="25%">Total</th>
            </tr>
            @foreach($invoice_data->items as $item)
            <tr>
                <td style="border: 1px solid; padding:12px;text-align:center;vertical-align:middle;">{{$item->name}}
                </td>
                <td style="border: 1px solid; padding:12px;text-align:center;vertical-align:middle;">{{$item->noOfItem}}
                </td>
                <td style="border: 1px solid; padding:12px;text-align:center;vertical-align:middle;">{{$item->price}}
                </td>
                <td style="border: 1px solid; padding:12px;text-align:center;vertical-align:middle;">{{$item->total}}
                </td>
            </tr>
            @endforeach
        </table>
        <table width="100%" class="mt-5 mb-5" style="border-collapse:collapse;border:0px;">
            <tr>
                <td></td>
                <td></td>
                <td style="border: 1px solid; padding:12px;text-align:center;vertical-align:middle;" width="25%">Sub Total</td>
                <td style="border: 1px solid; padding:12px;text-align:center;vertical-align:middle;" width="25%">
                {{$invoice_data->sub_total}}</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td style="border: 1px solid; padding:12px;text-align:center;vertical-align:middle;" width="25%">Tax</td>
                <td style="border: 1px solid; padding:12px;text-align:center;vertical-align:middle;" width="25%">
                {{$invoice_data->tax}}</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td style="border: 1px solid; padding:12px;text-align:center;vertical-align:middle;" width="25%">Grand Total</td>
                <td style="border: 1px solid; padding:12px;text-align:center;vertical-align:middle;" width="25%">
                {{$invoice_data->grand_total}}</td>
            </tr>
        </table>
    </div>
    </body>
</html>
