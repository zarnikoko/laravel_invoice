<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Invoice;
use App\Models\Item;
use Illuminate\Support\Facades\Input;
use DB;
use Response;

class invoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $invoice = Invoice::orderBy('updated_at','desc')->paginate(5);
        // echo '<pre>' . var_export($invoice->total(), true) . '</pre>';
        // highlight_string("<?php\n\$data =\n" . var_export($invoice, true) . "; \n? >");
        // exit();
        return view('index',[
            'invoice' => $invoice
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('newInvoice');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input   = Input::all();
        $invoice = new Invoice;
        $invoice->name = $request->invoiceName;
        $invoice->sub_total = $request->subTotal;
        $invoice->tax = $request->tax;
        $invoice->grand_total = $request->gTotal;
        $invoice->save();
        $invoice_id = $invoice->id;
        $itemName = $input['itemName'];
        foreach($itemName as $key => $val){
            $item = new Item;
            $item->invoice_id = $invoice_id;
            $item->name = $input['itemName'][$key];
            $item->noOfItem = $input['noOfItems'][$key];
            $item->price = $input['price'][$key];
            $item->total = $input['total'][$key];
            $item->save();
        }
        return back()->with('info','Invoice created successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $invoice = Invoice::findOrFail($id);
        return view('invoice',[
            'invoice_data' => $invoice
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $invoice = Invoice::findOrFail($id);
        return view('editInvoice',[
            'invoice' => $invoice
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $input   = Input::all();
         $invoice = Invoice::findOrFail($id);
         $invoice->items->each->delete();
         $invoice->name = $request->invoiceName;
         $invoice->sub_total = $request->subTotal;
         $invoice->tax = $request->tax;
         $invoice->grand_total = $request->gTotal;
         $invoice->save();
         $invoice_id = $invoice->id;
         $itemName = $input['itemName'];
            foreach($itemName as $key => $val){
                $item = new Item;
                $item->invoice_id = $invoice_id;
                $item->name = $input['itemName'][$key];
                $item->noOfItem = $input['noOfItems'][$key];
                $item->price = $input['price'][$key];
                $item->total = $input['total'][$key];
                $item->save();
            }
            return redirect()->route('index')->with('info','Invoice '.$invoice->name.' updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Invoice::Where('id',$id)->delete();
        return back()->with('info','Invoice deleted successfully');
    }
}
