<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table = 'invoice';

    protected $fillable =['name','sub_total','tax','grand_total','created_at','updated_at'];


     public function items()
    {
        return $this->hasMany('App\Models\Item','invoice_id','id');
    }
   
}
