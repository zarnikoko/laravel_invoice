<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Invoice;
use App\Models\Item;
use Illuminate\Support\Facades\Input;
use DB;
use Response;

class ApiInvoiceController extends Controller
{
    public function store_inv(Request $request)
    {
        $input   = Input::all();
        $invoice = $input['invoice'];
        $items = $input['items'];
        DB::beginTransaction();
        try {
            $id = DB::table('invoice')->insertGetId([
                      'name' => $invoice['invoiceName']
                    , 'sub_total' => $invoice['subTotal']
                    , 'tax' => $invoice['tax']
                    , 'grand_total' => $invoice['grandTotal']
                ]);
            foreach($items as $key => $val){
                DB::table('item')->insert([
                     'invoice_id' => $id 
                    ,'name' => $val['itemName'] 
                    ,'noOfItem' => $val['noOfItems']
                    ,'price' => $val['price']
                    ,'total' => $val['total']
                ]);
            } 
            DB::commit();
            return response()->json(compact('input'),202);
            // all good
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['error'=>$e->getMessage()],500);
        }
        
    }
    public function get_invs(Request $request){
        $sort =  json_decode($request['sort'],true);
        $limit = $request['limit'];
        $name = $sort['name'];
        $type = $sort['type'];
        $sort_table = "invoice.";
        if($name == "noOfItems"){
            $sort_table = "";
        }
        $invoices = DB::table('invoice')
                    ->leftJoin('item','item.invoice_id','=','invoice.id')
                    ->select(
                         'invoice.id as id'
                        ,'invoice.name as name'
                        ,'invoice.grand_total as grand_total'
                        ,DB::raw("DATE_FORMAT(invoice.updated_at, '%d-%m-%Y %r') as updated_at")
                        ,DB::raw("count(item.id) as noOfItems")
                    )
                    ->groupBy('invoice.id')
                    ->orderBy($sort_table.$name,$type)
                    ->paginate($limit);
    	return response()->json(compact('invoices'),202);
    }

    public function edit_inv(Request $request)
    {
        $id = $request->id;
        $invoice = Invoice::findOrFail($id);
        $invoice->items;
        return response()->json(compact('invoice'),202);
    }

    public function update_inv(Request $request){
        $id = $request->id;
        $invoice_request = $request->invoice;
        $itemName = $invoice_request['invoiceName'];
        $subTotal = $invoice_request['subTotal'];
        $tax =  $invoice_request['tax'];
        $grandTotal = $invoice_request['grandTotal'];
        DB::beginTransaction();
        try{
            $invoice = DB::table('invoice')->find($id);
            if($invoice){
                DB::table('item')->where('invoice_id', $id)->delete();
                DB::table('invoice')->where('id',$id)
                                    ->update([
                                        'name' => $itemName
                                       ,'sub_total' => $subTotal
                                       ,'tax' => $tax
                                       ,'grand_total' => $grandTotal
                                       ,'updated_at' => DB::raw('CURRENT_TIMESTAMP')
                                    ]);
                $items = $request->items;
                foreach($items as $key => $val){
                    DB::table('item')->insert([
                         'invoice_id' => $id 
                        ,'name' => $val['itemName'] 
                        ,'noOfItem' => $val['noOfItems']
                        ,'price' => $val['price']
                        ,'total' => $val['total'] 
                    ]);
                } 
            }else{
                return response()->json(['msg'=>'Invoice not found'],404);
            }
            DB::commit();
            return response()->json(compact('invoice'),202);
        }catch (\Exception $e){
            DB::rollback();
            $msg = ['error'=>$e->getMessage()];
            return response()->json(compact('msg','request'),500);
        }
    }

    public function destory_inv(Request $request){
    	$id = $request->id;
    	$res = Invoice::Where('id',$id)->delete();
    	if($res){
    		return response()->json(['msg'=>'Invoice deleted successfully!'],202);
    	}else{
    		return response()->json(['msg'=>'Error in deleting .Fail!'],500);
    	}
    }
}
